const mongoose=require("mongoose")
const {Schema}=mongoose
const PostSchema=new Schema({
    title:{
        type:String,
        trim:true
    },
    Content:{
        type:String
    },
    Image:{
        type:Buffer,
        contentType:String
    }
},{timestamps:true})